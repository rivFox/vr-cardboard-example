using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    private void Awake()
    {
        if(Instance != null)
        {
            Debug.LogError("Only one instance of PlayerController can exist!");
            return;
        }
        Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public void DealDamage()
    {

    }

}
