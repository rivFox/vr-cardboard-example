using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBehaviour : MonoBehaviour, ICameraPointerInteractable
{
    public void OnPointerClick()
    {
        Debug.Log("Click", gameObject);
    }

    public void OnPointerEnter()
    {
        Debug.Log("Enter", gameObject);
    }

    public void OnPointerExit()
    {
        Debug.Log("Exit", gameObject);
    }

    public void OnPointerStay()
    {
        Debug.Log("Stay", gameObject);
    }
}
