using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    [SerializeField] Transform _playerRoot;
    [SerializeField] GameObject _markerPrefab;
    [SerializeField] float _maxMarkerDistance = 10f;
    GameObject _currentMarker;
    
    private void Update()
    {
        float enter;
        var plane = new Plane(Vector3.up, Vector3.zero);
        var ray = new Ray(transform.position, transform.forward);

        if(plane.Raycast(ray, out enter) && enter <= _maxMarkerDistance)
        {
            SetMarkerPosition(ray.GetPoint(enter));
        }
        else
        {
            SetMarkerPosition(setActive: false);
        }

        // Checks for screen touches.
        if (Google.XR.Cardboard.Api.IsTriggerPressed || Input.GetMouseButtonDown(0))
        {
            if (_currentMarker && _currentMarker.activeSelf)
                SetPlayerPositionXZ(_currentMarker.transform.position);
        }
    }

    private void SetMarkerPosition(Vector3 position = default, bool setActive = true)
    {
        if (!_currentMarker)
            _currentMarker = Instantiate(_markerPrefab);

        if(setActive)
            _currentMarker.transform.position = position;

        _currentMarker.SetActive(setActive);
    }

    private void SetPlayerPositionXZ(Vector3 position)
    {
        _playerRoot.position = new Vector3(position.x, _playerRoot.position.y, position.z);
    }
}
