using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour, ICameraPointerInteractable
{
    static Vector3 PlayerPosition => PlayerController.Instance.transform.position;
    [SerializeField] Vector2 _spawnDistanceRange;
    [SerializeField] float _velocity = 1f;

    [Header("Health System")]
    [SerializeField] Slider _hpBarSlider;
    [SerializeField] float _maxHP;
    [SerializeField] float _currentHP;
    public float CurrentHealth
    {
        get => _currentHP;
        private set
        {
            _currentHP = value;
            _hpBarSlider.value = Mathf.Clamp01(_currentHP / _maxHP);
        }
    }

    [ContextMenu("Spawn")]
    public void Spawn()
    {
        CurrentHealth = _maxHP;

        var distance = Random.Range(_spawnDistanceRange.x, _spawnDistanceRange.y);
        var direction = new Vector3(Random.Range(-1f, 1f), Random.Range(0f, 1f), Random.Range(-1f, 1f));
        direction.Normalize();

        transform.position = PlayerPosition + direction * distance;
    }

    public void DealDamage(float value)
    {
        CurrentHealth -= value;
        if (_currentHP <= 0f)
        {
            Kill();
            return;
        }
    }

    public void Kill()
    {
        Spawn();
    }

    private void Start()
    {
        Spawn();
    }

    private void Update()
    {
        var targetPosition = PlayerPosition;
        var direction = (targetPosition - transform.position).normalized;
        transform.Translate(direction * _velocity * Time.deltaTime, Space.World);
        transform.forward = direction;

        if (Vector3.Distance(transform.position, targetPosition) <= 0.1f)
        {
            PlayerController.Instance.DealDamage();
            Kill();
        }
    }

    #region ICameraPointerInteractable
    public void OnPointerClick()
    {
    }

    public void OnPointerEnter()
    {
    }

    public void OnPointerExit()
    {
    }

    public void OnPointerStay()
    {
        DealDamage(Time.deltaTime);
    }
    #endregion
}
